from collections import deque
from datetime import datetime
import unittest

from main import Processor, SlidingWindow

class TestSlidingWindowMethods(unittest.TestCase):

    def test_append_works_with_one_initial_entry(self):
        sliding_window = SlidingWindow()
        first = datetime(2018, 1, 1, 23, 1, 38, 1000)
        sliding_window.window = deque([first])
        sliding_window.count = 1
 
        second = datetime(2018, 1, 1, 23, 3, 3, 8000)
        sliding_window.append(second)
        self.assertEqual(sliding_window.count, 2)
        self.assertEqual(sliding_window.window, deque([first, second]))

    def test_append_works_with_two_initial_entries(self):
        sliding_window = SlidingWindow()
        first = datetime(2018, 1, 1, 23, 1, 38, 1000)
        second = datetime(2018, 1, 1, 23, 3, 3, 8000)
        sliding_window.window = deque([first, second])
        sliding_window.count = 2

        third = datetime(2018, 1, 1, 23, 3, 5, 9000)
        sliding_window.append(third)
        self.assertEqual(sliding_window.count, 2)
        self.assertEqual(sliding_window.window, deque([second, third]))

    def test_first_works(self):
        sliding_window = SlidingWindow()
        first = datetime(2018, 1, 1, 23, 1, 38, 1000)
        second = datetime(2018, 1, 1, 23, 3, 3, 8000)
        sliding_window.window = deque([first, second])
        sliding_window.count = 2

        result = sliding_window.first()
        self.assertEqual(result, first)

    def test_slide_works(self):
        sliding_window = SlidingWindow()
        first = datetime(2018, 1, 1, 23, 1, 38, 1000)
        second = datetime(2018, 1, 1, 23, 3, 3, 8000)
        sliding_window.window = deque([first, second])
        sliding_window.count = 2

        sliding_window.slide()
        self.assertEqual(sliding_window.count, 1)
        self.assertEqual(sliding_window.window, deque([second]))


class TestProcessorMethods(unittest.TestCase):

    def test_generate_alert_generates_alert_for_three_consecuctive_entries_within_five_minutes(self):
        processor = Processor()
        sliding_window = SlidingWindow()
        sliding_window.window = deque([datetime(2018, 1, 1, 23, 1, 38, 1000), datetime(2018, 1, 1, 23, 3, 3, 8000)])
        sliding_window.count = 2
        processor.sliding_windows = {"1000|TSTAT|RED HIGH": sliding_window}
    
        line = "20180101 23:03:05.009|1000|101|98|25|20|101.2|TSTAT"
        expected = {
            "satelliteId": 1000,
            "severity": "RED HIGH",
            "component": "TSTAT",
            "timestamp": "2018-01-01T23:01:38.001Z"
        }
        alert = processor.generate_alert(line)
        print(alert)

        print(expected)
        self.assertEqual(alert, expected)

    def test_generate_alert_does_not_gemerate_alert_for_when_one_of_three_is_not_within_five_minutes(self):
        processor = Processor()
        sliding_window = SlidingWindow()
        sliding_window.window = deque([datetime(2018, 1, 1, 23, 1, 38, 1000), datetime(2018, 1, 1, 23, 3, 3, 8000)])
        sliding_window.count = 2
        processor.sliding_windows = {"1000|TSTAT|RED HIGH": sliding_window}
     
        line = "20180101 23:10:05.009|1000|101|98|25|20|101.2|TSTAT"
        alert = processor.generate_alert(line)
        self.assertIsNone(alert)

if __name__ == '__main__':
    unittest.main()