from collections import deque
from datetime import datetime, timedelta
import argparse
import json

# define severity levels constants
RED_HIGH_SEVERITY = "RED HIGH"
RED_LOW_SEVERITY = "RED LOW"
YELLOW_HIGH_SEVERITY = "YELLOW HIGH"
YELLOW_LOW_SEVERITY = "YELLOW LOW"

# define components name constants
BATTERY_COMPONENT = "BATT"
THERMOSTAT_COMPONENT = "TSTAT"

# data structures to track entries that meet the requirements
class SlidingWindow:
    def __init__(self):
        self.count = 0
        self.window = deque()
    def first(self):
        return self.window[0]
    def append(self, timestamp):
        self.window.append(timestamp)
        self.count += 1
        if self.count == 3:
            self.window.popleft()
            self.count -= 1
    def slide(self):
        self.window.popleft()
        self.count -= 1

class Processor:
    def __init__(self):
        self.sliding_windows = dict()
    def generate_alert(self, line):
        """Takes an input line, returns a alert (dict) if line matches requirements else None."""
        alert = None
        timestamp, satellite_id, red_high_limit, yellow_high_limit, yellow_low_limit, red_low_limit, raw_value, component = line.split("|")
        satellite_id = int(satellite_id)
        red_high_limit = float(red_high_limit)
        red_low_limit = float(red_low_limit)
        yellow_high_limit = float(yellow_high_limit)
        yellow_low_limit = float(yellow_low_limit)
        raw_value = float(raw_value)

        # determine severity
        severity = None
        if raw_value >= red_high_limit:
            severity = RED_HIGH_SEVERITY
        elif raw_value >= yellow_high_limit:
            severity = YELLOW_HIGH_SEVERITY
        elif raw_value <= red_low_limit:
            severity = RED_LOW_SEVERITY
        elif raw_value <= yellow_low_limit:
            severity = YELLOW_LOW_SEVERITY

        # check requirement for match
        if ( (component == BATTERY_COMPONENT and severity == RED_LOW_SEVERITY)
            or (component == THERMOSTAT_COMPONENT and severity == RED_HIGH_SEVERITY) ):
            key = f'{satellite_id}|{component}|{severity}'
            current_timestamp = datetime.strptime(timestamp, '%Y%m%d %H:%M:%S.%f')
            if key in self.sliding_windows:
                sliding_window = self.sliding_windows[key]
                first_timestamp = sliding_window.first()
                if current_timestamp - first_timestamp > timedelta(minutes=5):
                    window = self.sliding_windows[key]
                    window.slide()
                    window.append(current_timestamp)
                elif sliding_window.count == 2:
                    first_timestamp_str = first_timestamp.strftime("%Y-%m-%dT%H:%M:%S.%f")
                    first_timestamp_str = first_timestamp_str[0:len(first_timestamp_str) - 3] + "Z"
                    alert = {
                        "satelliteId": satellite_id,
                        "severity": severity,
                        "component": component,
                        "timestamp": first_timestamp_str
                    }
                    sliding_window.append(current_timestamp)
                else:
                    sliding_window.append(current_timestamp)
            else:
                window = SlidingWindow()
                window.append(current_timestamp)
                self.sliding_windows[key] = window

        return alert


def main():
    parser = argparse.ArgumentParser(description='Parses input file stream and alerts for anomalies.')
    parser.add_argument('pfile', action='store', type=argparse.FileType('r'), help='path of file to parse.')
    args = parser.parse_args()

    processor = Processor()
    output = []
    while True:
        line = args.pfile.readline().rstrip()
        if line == "":
            break
        alert = processor.generate_alert(line)
        if alert:
            output.append(alert)

    print(json.dumps(output, indent=4))

if __name__ == '__main__':
    main()